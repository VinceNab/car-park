<?php include_once 'fixed/header.php'; ?>
<div class="main">
    <table class="table">
        <thead>
            <th>Client</th>
            <th>Vehicle</th>
            <th>Space</th>
            <th>Rate(12hrs)</th>
            <th>Time In</th>
            <th>Time Out</th>
            <th>Total</th>
            <th>Payment Status</th>
        </thead>
        <tbody>
            <tr>
                <?php
                $alloc =  $dbfetch->general_fetch('allocations');
                foreach ($alloc as $rent) {
                    $pay = $rent['payment_status'];
                ?>
                    <td><?= $rent['client_name']; ?></td>
                    <td><?= $rent['car_reg']; ?></td>
                    <td><?= $rent['lot_assigned']; ?></td>
                    <td><?= $rent['lot_rate']; ?></td>
                    <td><?= $rent['time_in']; ?></td>
                    <td><?= $rent['time_out']; ?></td>
                    <td><?= $rent['total']; ?></td>
                    <?php
                     if($pay == 0){
                        ?>
                        <td>Not Paid</td>
                        <?php
                     }elseif($pay == 1){
                        ?>
                            <td>Paid</td>
                        <?php
                    }
                }
                ?>
            </tr>
        </tbody>
    </table>
</div>

<?php include_once 'fixed/footer.php'; ?>
<?php
include_once 'fixed/header.php';
$errors = null;

//Registration
if (isset($_POST['new_registration'])) {

    $pass1 = $_POST['password'];
    $pass2 = $_POST['cpassword'];
    if ($pass1 != $pass2) {
        $errors = "Password 1 and 2 don't match!!!!";
    } else {
        $user = $_POST['username'];
        $mail = $_POST['email'];


        $SELECT = "SELECT * FROM staff WHERE staff_mail = ? and staff_name = ? Limit 1";
        $stmt = $connect->prepare($SELECT);
        $stmt->bind_param("ss", $mail, $user);
        $stmt->execute();
        $results = $stmt->get_result();
        $rnum = $results->num_rows;

        if ($rnum === 0) {
            $phone = $_POST['phone'];
            $gender = $_POST['gender'];
            $passwd = password_hash($pass1, PASSWORD_DEFAULT);
            $token = bin2hex(random_bytes(50));
            $dates = date('yy/m/d');

            $insert = "INSERT INTO staff (staff_name, staff_gender, staff_mail, phone, employment_date, Token, user_password) VALUES(?, ?, ?, ?, ?, ?, ?)";
            $stmt1 = $connect->prepare($insert);
            $stmt1->bind_param("sssssss", $user, $gender, $mail, $phone, $dates, $token, $passwd);
            if ($stmt1->execute()) {
                echo 'Data Saved';
                $errors .= '<div class="card">Data saved Successfully</div>';

                $stmt1->close();
            } else {
                echo 'Unable to save';
            }
        } else {
            $errors .= "User already registered";
        }
    }
}

?>
<div class="main">
    <center>
        <h2>Add new system user</h2>
    </center>
    <div class="signup-form">
        <form action="" method="POST">
            <div class="text-center" style="color: red;">
                <?= $errors; ?>
            </div>
            <div class="form-group">
                <label>Username (First and Last) <b class="redstar">*</b></label>
                <input type="text" class="form-control" name="username" required="required">
            </div>
            <div>
                <label for="formFileMultiple" class="form-label">Gender <b class="redstar">*</b></label>
                <select name="gender" class="form-select" aria-label="Disabled select example" required>
                    <option selected value="">~Select Gender~</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Other">Other</option>
                </select>
            </div>
            <div class="form-group">
                <label>Phone Number<b class="redstar">*</b></label>
                <input type="text" class="form-control" name="phone" required="required" placeholder="07........." minlength="10">
            </div>
            <div class="form-group">
                <label>Email Address <b class="redstar">*</b></label>
                <input type="email" class="form-control" name="email" required="required" placeholder="abc@gmail.com">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" required="required" placeholder="*************************" id="pass1" minlength="8">
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control" name="cpassword" required="required" placeholder="*************************" id="pass2" minlength="8">
            </div>
            <div class="form-group">
                <center>
                    <button type="submit" name="new_registration" class="btn btn-primary btn-block btn-lg mt-2">Add User</button>
                </center>
            </div>
        </form>
    </div>
</div>

<?php include_once 'fixed/footer.php'; ?>
<?php
// Defining constants
define('dbhost','localhost');
define('dbname','parking');
define('dbusername','root');
define('dbpassword','');

// creating connection
$connect = new mysqli(dbhost,dbusername,dbpassword,dbname);
if($connect->connect_error){
    die('Database error' + $connect->connect_error);
}
?>
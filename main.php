<?php
session_start();
require 'auth/config.php';

// Update user details
if (isset($_POST['update_details'])) {
    echo 'logout';
}
// New lot
elseif (isset($_POST['register_lot'])) {
    $lot_name = $_POST['lotname'];
    $lot_rate = $_POST['lotcharges'];
    $care_taker = $_POST['p_incharge'];

    // Checking if entry already exist
    $SELECT = "SELECT * FROM lots WHERE lot_name = ? AND lot_rate = ? Limit 1";
    $stmt = $connect->prepare($SELECT);
    $stmt->bind_param("ss", $lot_name, $lot_rate);
    $stmt->execute();
    $reslts = $stmt->get_result();
    $rnum = $reslts->num_rows;

    if ($rnum === 0) {
        $insert = "INSERT INTO lots (lot_name, lot_rate, person_incharge) VALUES (?, ?, ?)";
        $stmt = $connect->prepare($insert);
        $stmt->bind_param("sss", $lot_name, $lot_rate, $care_taker);
        if ($stmt->execute()) {
            ?>
            <script>
                alert('Save Successfully');
            </script>
        <?php
            header("refresh: 1; home.php");
        } else {
        ?>
            <script>
                alert('Not saved try again');
            </script>
        <?php
            header('location: home.php');
        }
    } else {
        ?>
        <script>
            alert('Entry already exists');
        </script>
        <?php
        header("refresh: 1; home.php");
    }
}

// Lot allocation
elseif (isset($_POST['new_allocationh'])) {
    $client_name = $_POST['username'];
    $phone_number = $_POST['phone'];
    $car_reg = $_POST['car'];
    $space_rate = $_POST['lot_assigned'];
    $time_in = date('Y-m-d H:i:s');

    // getting rate and lot
    $temp = explode('_', $space_rate);
    $space = $temp[0];
    $rate = $temp[1];

    $SELECT = "SELECT * FROM allocations WHERE client_name = ? AND time_in = ? Limit 1";
    $stmt = $connect->prepare($SELECT);
    $stmt->bind_param("ss",$client_name, $time_in);
    $stmt->execute();
    $reslts = $stmt->get_result();
    $rnum = $reslts->num_rows;

    if ($rnum === 0) {
        // $stmt->close();
        $total = $rate;
        $payment = 0;
        $INSERT = "INSERT INTO allocations (client_name, client_phone, car_reg, lot_assigned, lot_rate, total, payment_status, time_in) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $connect->prepare($INSERT);
        $stmt->bind_param("ssssssss", $client_name, $phone_number, $car_reg, $space, $rate, $total, $payment, $time_in);
        if($stmt->execute()){
            ?>
            <script>
                    alert('Successfully Allocated');
            </script>
            <?php
            header("refresh: 1",'home.php');
        }else{
            ?>
            <script>
                    alert('Allocation was not successfull');
            </script>
            <?php
            header("refresh: 1",'home.php');
        }
        
        $stmt0->close();
    }
    // header('location: home.php');
}
//Log out of the systema
elseif (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['id']);
    unset($_SESSION['user_key']);
    unset($_SESSION['username']);

    header('location: index.php');
    exit();
}

<?php
require('main.php');
$errors = null;

if (isset($_SESSION['id'])) {
    header('location: home.php');
} elseif (isset($_POST['Request'])) {
    $pEml = $_POST['email'];
    $Pswd = $_POST['password'];

    $SELECT = "SELECT * FROM staff WHERE staff_mail =? LIMIT 1";
    $stmt = $connect->prepare($SELECT);
    $stmt->bind_param("s", $pEml);
    $stmt->execute();
    $reslts = $stmt->get_result();
    $rnum = $reslts->num_rows;
    $user_m = $reslts->fetch_assoc();

    if (password_verify($Pswd, $user_m['user_password'])) {
        $_SESSION['id'] = $user_m['Token'];
        $_SESSION['username'] = $user_m['staff_name'];
        $_SESSION['userkey'] = $user_m['staff_id'];

        header('location: home.php');
        $stmt->close();
        exit();
    } else {
        $errors = 'Incorrect email address or Password';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Car Parking System</title>
    <link rel="stylesheet" href="index.css" />
    <link rel="icon" href="images/download.jpg" type="image/x-icon" />
    <link rel="stylesheet" href="main.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <div class="main">
            <div class="container">
                <center>
                    <h1 class="mt-5 s_heading">Car Parking System</h1>
                </center>
            </div>
            <div class="container cenitem">
                <div>
                    <center>
                        <h4 class="mb-2">Log in page</h4>
                    </center>
                </div>
                <div class="card signup-form w-50 shadow-lg mx-auto">
                    <form action="" method="post" class="card-body">
                        <div class="text-center" style="color: red;">
                            <?= $errors; ?>
                        </div>
                        <div class="form-group">
                            <label>Email Address <b class="redstar">*</b></label>
                            <input type="email" class="form-control" name="email" placeholder="abc@gmail.com" required="required">
                        </div>
                        <div class="form-group">
                            <label>Password <b class="redstar">*</b></label>
                            <input type="password" class="form-control" name="password" placeholder="***************" required="required" minlength="5">
                        </div>
                        <div class="form-group d-flex justify-content-between mt-2">
                            <div>
                                <button type="submit" class="btn btn-lg bg-primary" name="Request">Log In</button>
                            </div>
                            <div class="mt-1">
                                <a href="#">Forgot password ?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include_once 'fixed/header.php';
if (isset($_POST['new_allocation'])) {
    $client_name = $_POST['username'];
    $phone_number = $_POST['phone'];
    $car_reg = $_POST['car'];
    $space_rate = $_POST['lot_assigned'];
    $time_in = date('Y-m-d H:i:s');
    $payment = 0;

    // getting rate and lot
    $temp = explode('_', $space_rate);
    $space = $temp[0];
    $rate = $temp[1];
    $total = $rate;

    $SELECT = "SELECT * FROM allocations WHERE client_name = ? AND lot_assigned = ?  Limit 1";
    $stmt = $connect->prepare($SELECT);
    $stmt->bind_param("ss", $client_name, $space);
    $stmt->execute();
    $reslts = $stmt->get_result();
    $rnum = $reslts->num_rows;

    if ($rnum === 0) {
        $INSERT = "INSERT INTO allocations (client_name, clinet_phone, car_reg, lot_assigned, lot_rate, total, payment_status, time_in) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $connect->prepare($INSERT);
        $stmt->bind_param("ssssssss", $client_name, $phone_number, $car_reg, $space, $rate, $total, $payment, $time_in);
        if ($stmt->execute()) {
            $stmt->close();
            $update_lots = "UPDATE lots SET lot_status = ? WHERE lots.lot_name = '$space'";
            $activate = 1;
            $stmt = $connect->prepare($update_lots);
            $stmt->bind_param("i", $activate);
            $stmt->execute();
?>
            <script>
                alert('Lot allocated Successfully');
            </script>
<?php
        }
    }
}
?>

<div class="main">
    <!-- Statistics -->
    <section class="card shadow-lg mt-2 mb-2">
        <div class="d-flex justify-content-between mt-2 mb-2 text-center">
            <button class="btn btn-md bg-light shadow-lg" data-bs-toggle="modal" data-bs-target="#userModal">
                <div class="card p1 shadow bg-primary infoitm" id="itm1">
                    <h4 class="card-title"><?= $dbfetch->Users(); ?></h4>
                    <h6 class="card-title">All Users</h6>
                </div>
            </button>
            <button class="btn btn-md bg-light shadow-lg" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <!-- Onclick a pop up appears -->
                <div class="card p1 shadow bg-primary infoitm">
                    <h5 class="card-title">+</h5>
                    <h6 class="card-title text-center">New Space</h6>
                </div>
            </button>
            <!-- <div class="btn btn-md bg-light shadow-lg">
                <div class="card p1 shadow bg-primary infoitm">
                    <h4 class="card-title">25</h4>
                    <h6 class="card-title">Cars Parked</h6>
                </div>
            </div> -->
            <div class="btn btn-md bg-light shadow-lg">
                <div class="card p1 shadow bg-primary infoitm">
                    <h3 class="card-title"><?= $dbfetch->freeSpaces('0'); ?></h3>
                    <h6 class="card-title">Space Available</h6>
                </div>
            </div>
            <div class="btn btn-md bg-light shadow-lg">
                <div class="card p1 shadow bg-danger infoitm">
                    <h3 class="card-title"><?= $dbfetch->freeSpaces('1'); ?></h3>
                    <h6 class="card-title">Space Occupied</h6>
                </div>
            </div>
        </div>
    </section>
    <!-- Users Modal -->
    <div class="modal fade" id="userModal" tabindex="-1" aria-labelledby="exampleModalLabelh" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ALL Users</h5>
                    <button type="button" class="btn-close bg-danger" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <?php
                        $allUser = $dbfetch->allUsers();
                        foreach ($allUser as $user) {
                        ?>
                            <p>~<?= $user['staff_name']; ?></p>
                        <?php

                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Lot Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add new parking lot</h5>
                    <button type="button" class="btn-close bg-danger" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body signup-form">
                    <form action="main.php" method="post">
                        <div class="form-group">
                            <label>Lot Name<b class="redstar"> *</b></label>
                            <input type="text" class="form-control" name="lotname" required="required">
                        </div>
                        <div class="form-group">
                            <label>Lot Charges<b class="redstar"> *</b></label>
                            <input type="text" class="form-control" name="lotcharges" required="required">
                        </div>
                        <div>
                            <label for="formFileMultiple" class="form-label">Person Incharge</label>
                            <select name="p_incharge" class="form-select" aria-label="Disabled select example" required>
                                <option selected value="0">~Select Person incharge~</option>
                                <option value="james">James</option>
                            </select>
                        </div>
                        <div class="form-group mt-2">
                            <center>
                                <button type="submit" class="btn btn-primary btn-lg" name="register_lot">Add Space</button>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Action tabs -->
    <div>
        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#newallocation" type="button" role="tab" aria-controls="home" aria-selected="true">New Allocation</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#allocationdetails" type="button" role="tab" aria-controls="profile" aria-selected="false">Allocations</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#freelots" type="button" role="tab" aria-controls="contact" aria-selected="false">All Spaces</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="payment-tab" data-bs-toggle="tab" data-bs-target="#payments" type="button" role="tab" aria-controls="contact" aria-selected="false">Payments</button>
            </li>
        </ul>
    </div>
    <div class="tab-content" id="myTabContent">
        <!-- New lot allocation -->
        <div class="tab-pane fade show active" id="newallocation" role="tabpanel" aria-labelledby="home-tab">
            <div class="mb-5 mt-2">
                <div class="signup-form">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label>Client Name(First & Last)<b class="redstar"> *</b></label>
                            <input type="text" class="form-control" name="username" required="required">
                        </div>
                        <div class="form-group">
                            <label>Phone Number<b class="redstar"> *</b></label>
                            <input type="text" class="form-control" name="phone" required="required" placeholder="0712..........." minlength="10" maxlength="12">
                        </div>
                        <div class="form-group">
                            <label>Car Registration number<b class="redstar"> *</b></label>
                            <input type="text" class="form-control" name="car" required="required" placeholder="KCA 125B">
                        </div>

                        <div>
                            <label for="formFileMultiple" class="form-label">Lot allocated</label>
                            <select name="lot_assigned" class="form-select" aria-label="Disabled select example" required>
                                <option selected value="">~Select Lot~</option>
                                <?php
                                $lots = $dbfetch->freeLot();
                                foreach ($lots as $Lot) {
                                ?>
                                    <option value="<?= $Lot['lot_name'] . '_' . $Lot['lot_rate']; ?>"><?= $Lot['lot_name']; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" hidden>
                            <label>Time in</label>
                            <input type="text" class="form-control" name="time_in" required="required" disabled value="<?= date('Y-m-d h:m:s'); ?>">
                        </div>
                        <div class="form-group d-flex justify-content-between mt-2">
                            <button type="submit" class="btn btn-primary btn-lg" name="new_allocation">Allocate</button>
                            <button type="reset" class="btn btn-danger btn-lg">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Allocations made and making payments -->
        <div class="tab-pane fade" id="allocationdetails" role="tabpanel" aria-labelledby="profile-tab">
            <section>
                <table class="table">
                    <thead>
                        <th>Car Owner</th>
                        <th>Car Registration</th>
                        <th>Lot Assigned</th>
                        <th>Time In</th>
                        <th>Total</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php
                        $alloc =  $dbfetch->allocation();
                        foreach ($alloc as $rent) {
                            $pay = $rent['payment_status'];
                        ?>
                            <tr>
                                <td><?= $rent['client_name']; ?></td>
                                <td><?= $rent['car_reg']; ?></td>
                                <td><?= $rent['lot_assigned']; ?></td>
                                <td><?= $rent['time_in']; ?></td>
                                <td><?= $rent['total']; ?></td>
                                <td><a href="">Check out</a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </section>
        </div>
        <!-- All lots -->
        <div class="tab-pane fade" id="freelots" role="tabpanel" aria-labelledby="contact-tab">
            <section class="gView mt-2">
                <?php
                $allots = $dbfetch->allLot();
                foreach ($allots as $Lot) {
                    $availability = $Lot['lot_status'];
                    if ($availability == 0) {
                ?>
                        <div class="card p1 shadow bg-primary slot text-center">
                            <h5 class="card-title"><?= $Lot['lot_name']; ?></h5>
                            <p class="mt-0 mb-0"><?= $Lot['lot_rate']; ?>/=</p>
                            <p class="mt-0 mb-0">Available</p>
                        </div>
                    <?php
                    } else {
                    ?>
                        <div class="card p1 shadow bg-danger slot text-center">
                            <h5 class="card-title"><?= $Lot['lot_name']; ?></h5>
                            <p class="mt-0 mb-0"><?= $Lot['lot_rate']; ?>/=</p>
                            <p class="mt-0 mb-0">Occupied</p>

                        </div>
                <?php
                    }
                }
                ?>
            </section>

        </div>
        <!-- Payments -->
        <div class="tab-pane fade" id="payments" role="tabpanel" aria-labelledby="payment-tab">
            <table class="table">
                <thead>
                    <th>Client</th>
                    <th>Vehicle</th>
                    <th>Space</th>
                    <th>Rate(12hrs)</th>
                    <th>Time In</th>
                    <th>Time Out</th>
                    <th>Total</th>
                    <th>Payment Status</th>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        $alloc =  $dbfetch->allocation();
                        foreach ($alloc as $rent) {
                            $pay = $rent['payment_status'];
                        ?>
                            <td><?= $rent['client_name']; ?></td>
                            <td><?= $rent['car_reg']; ?></td>
                            <td><?= $rent['lot_assigned']; ?></td>
                            <td><?= $rent['lot_rate']; ?></td>
                            <td><?= $rent['time_in']; ?></td>
                            <td><?= $rent['time_out']; ?></td>
                            <td><?= $rent['total']; ?></td>
                            <?php
                            if ($pay == 0) {
                            ?>
                                <td>Not Paid</td>
                            <?php
                            } elseif ($pay == 1) {
                            ?>
                                <td>Paid</td>
                        <?php
                            }
                        }
                        ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php include_once 'fixed/footer.php'; ?>
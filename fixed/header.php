<?php require 'main.php';
// If you have not signed in you cannot access the system
// redirected to log in page
if(!isset($_SESSION['id'])){
    header('location: index.php');
}

// Fetching data functions
class FetchData
{
    function Users()
    {
        $users = "SELECT * FROM staff";
        global $connect;
        $stmt = $connect->prepare($users);
        $stmt->execute();
        $reslts = $stmt->get_result();
        $rnum = $reslts->num_rows;

        return $rnum;
    }
    function freeSpaces($a)
    {
        $users = "SELECT * FROM lots WHERE lot_Status = $a";
        global $connect;
        $results = $connect->query($users);
        $rnums = $results->num_rows;

        return $rnums;
    }
    function allUsers()
    {
        $users = "SELECT * FROM staff";
        global $connect;
        $results = $connect->query($users);
        $allusers = $results->fetch_all(MYSQLI_ASSOC);

        return $allusers;
    }

    function staff()
    {
        $staff = "SELECT * FROM staff";
        global $connect;
        $results = $connect->query($staff);
        $allstaff = $results->fetch_all(MYSQLI_ASSOC);

        return $allstaff;
    }
    function freeLot()
    {
        $freelots = "SELECT * FROM lots WHERE lot_status = 0";
        global $connect;
        $results = $connect->query($freelots);
        $allfree = $results->fetch_all(MYSQLI_ASSOC);

        return $allfree;
    }
    function allLot()
    {
        $freelots = "SELECT * FROM lots";
        global $connect;
        $results = $connect->query($freelots);
        $all = $results->fetch_all(MYSQLI_ASSOC);

        return $all;
    }
    
    function allocation(){
        $allocations = "SELECT * FROM allocations";
        global $connect;
        $results = $connect->query($allocations);
        $all = $results->fetch_all(MYSQLI_ASSOC);

        return $all;
    }   
    function userprofile($id){
        $users = "SELECT * FROM staff WHERE staff_name = '$id'";
        global $connect;
        $results = $connect->query($users);
        $user = $results->fetch_all(MYSQLI_ASSOC);

        return $user;
    }
}

$dbfetch = new FetchData();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Car Parking</title>
    <link rel="stylesheet" href="index.css" />
    <link rel="icon" href="images/lot.png" type="image/x-icon" />
    <link rel="stylesheet" href="main.css">
    <!-- Jquey -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper container">
        <div class="header">
            <nav class="navbar navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">
                        <img src="images/lot2.png" alt="" width="30" height="24" class="d-inline-block align-text-top">
                        Car Parking system
                    </a>
                    <div>
                        <ul class="nav justify-content-center">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="home.php">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="profile.php">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="adduser.php">Add User</a>
                            </li>
                            <li class="nav-item">
                                <form action="main.php" method="GET">
                                    <button class="btn btn-xs btn-outline-danger" name="logout">Log Out</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
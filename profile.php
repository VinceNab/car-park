<?php
include_once 'fixed/header.php';
$current_user = $_SESSION['username'];
// $id = $_SESSION['userkey'];

$errors = null;

// changing password
if (isset($_POST['passwordchange'])) {
    $pass = $_POST['oldpass'];
    $new_pass = $_POST['newpass'];
    $new_pass2 = $_POST['newpass2'];
    if ($new_pass !== $new_pass2) {
        $errors = "<p focus>The new passwords <b>do not match</b></p>";
    } else {

        $SELECT = "SELECT * FROM staff WHERE staff_name = ? LIMIT 1";
        $stmt = $connect->prepare($SELECT);
        $stmt->bind_param("s", $current_user);
        $stmt->execute();
        $reslts = $stmt->get_result();
        $rnum = $reslts->num_rows;
        $user_upd = $reslts->fetch_assoc();

        if (password_verify($pass, $user_upd['user_password'])) {
            $update = "UPDATE staff SET user_password = ? WHERE staff.staff_name = '$current_user'";
            $stmt = $connect->prepare($update);
            $stmt->bind_param("s", $new_pass);
            if ($stmt->execute()) {
                $errors .= '<div class="card bg-success" style="color: white !important;">Password Changes Successfully</div>';
                $stmt->close();
            }
        } else {
            $errors .= "The old password does not match <br> Old password " . $user_upd['user_password'];
            $stmt->close();
        }
    }
}
?>
<div class="main mb-5">
    <center>
        <div class="imgcontainer">
            <img src="images/P.png" alt="Avatar" class="avatar">
        </div>

        <div id="info">
            <?php
            $users = $dbfetch->userprofile($current_user);
            foreach ($users as $user) {
            ?>
                <p>Name: <?= $user['staff_name']; ?></p>
                <p>Employment ID: Emp_0<?= $user['staff_id']; ?></p>
                <p>Gender: <?= $user['staff_gender']; ?></p>
                <p>Contact: <?= $user['phone']; ?></p>
                <p>Email: <?= $user['staff_mail']; ?></p>
                <p>Employee From: <?= $user['employment_date']; ?></p>
            <?php
            }
            ?>
            <div class="card etcs" hidden>
                <p><a href="#">Change password</a></p>
                <p><a href="#">Update account details</a></p>
                <p><a href="#">Change subscription</a></p>
            </div>
        </div>
    </center>
    <div class="row g-2">
        <div class="col-4 card">
            <h6 class="text-center">Change password</h6>
            <div class="signup-form mb-2">
                <form action="" method="post">
                    <div class="text-center" style="color: red;">
                        <?= $errors; ?>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Old password</label>
                        <input type="password" class="form-control" name="oldpass" required="required">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">New password</label>
                        <input type="password" class="form-control" name="newpass" required="required" minlength="8">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Confirm password</label>
                        <input type="password" class="form-control" name="newpass2" required="required" minlength="8">
                    </div>
                    <center>
                        <button class="btn btn-lg bg-primary mt-2" name="passwordchange">Submit</button>
                    </center>
                </form>
            </div>
        </div>
        <div class="col-8 card">
            <h6 class="text-center">Update user details</h6>
            <div class="signup-form">
                <form action="main.php" method="POST">
                    <?php
                    // Fetching user profile using session name
                    $users = $dbfetch->userprofile($current_user);
                    foreach ($users as $user) {
                    ?>
                        <div class="form-group">
                            <label>Username (First and Last) <b class="redstar">*</b></label>
                            <input type="text" class="form-control" name="username" required="required" value="<?= $user['staff_name']; ?>">
                        </div>
                        <div>
                            <label for="formFileMultiple" class="form-label">Gender <b class="redstar">*</b></label>
                            <select name="gender" class="form-select" aria-label="Disabled select example" required>
                                <option selected value="<?= $user['staff_gender']; ?>"><?= $user['staff_gender']; ?></option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Phone Number<b class="redstar">*</b></label>
                            <input type="text" class="form-control" name="phone" required="required" value="<?= $user['phone']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Email Address <b class="redstar">*</b></label>
                            <input type="email" class="form-control" name="email" required="required" value="<?= $user['staff_mail']; ?>">
                        </div>
                    <?php
                    }
                    ?>
                    <div class="form-group">
                        <center>
                            <button type="submit" class="btn btn-primary btn-block btn-lg mt-2" name="updateDetails">Update</button>
                        </center>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<?php include_once 'fixed/footer.php'; ?>